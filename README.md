# Marvel Simple API

This project is a simplification of the [official Marvel API](https://developer.marvel.com). It lets you retrieve Marvel characters using a very simple REST syntax.

## Features

- Search for Marvel characters using the `/characters` route. Remember that it will only return characters whose names starts with your search query. You should use this route first in order to find the character you want,
- Get a Marvel character and the list of comics in which it appears using `/characters/{name}`. Name must be the exact name of the character as it was returned in the first route,
- Caching : all requests are cached for one day (because Marvel characters and comics don't change very often).

## Usage

To learn how to use this API, please use the OpenAPI documentation, located in `docs/openapi.json`. You can use this file to [generate a client](https://github.com/OpenAPITools/openapi-generator), [import it in Postman](https://learning.postman.com/docs/integrations/available-integrations/working-with-openAPI/) or simply [view it in a web format](https://editor.swagger.io/) (JSON files can also be copy-pasted in this editor).

## Running the app

### Prerequisites

- [Docker](https://docs.docker.com/engine/install/) (version 24 or newer, may work with older versions)
- [Docker Compose](https://docs.docker.com/compose/install/) (version 2 or newer) with the [Docker Compose CLI plugin](https://docs.docker.com/compose/install/linux/)

### Setting the environment variables

Copy the `.env.sample` file to `.env` and edit it.

You must specify your Marvel public and private keys, or else all calls to the Marvel API will fail.

You can retrieve a new Marvel API key on the [Marvel developer portal](https://developer.marvel.com). When creating your Marvel account, remember to add `localhost` and/or the hostname on which the app will be hosted to the list of allowed referers.

### Default port and changing it

By default, this app runs on the 3000 port. If you want to change it, edit the `docker-compose.prod.yml` file and change the port binding to `<port>:3000` (the line under `ports:`).

### Build and run the Docker image

Now that everything is set up, you can run the app using this command:

```sh
docker compose --file docker-compose.prod.yml up -d --build
```

If everything worked, you should now be able to send requests to it.

## Developing on the project

When developing on this project, you should use the development environment.

This environment has some key differences from the production environment:

- The whole project directory is mounted on the Docker Container, to get access to the same files as your local copy of the project, including the node modules
- The app is run in watch mode, meaning every edit you make leads to a reload of the app with your changes
- In development mode, NestJS is slower, meaning it must not be used in production

### Prerequisites

- [Docker](https://docs.docker.com/engine/install/) (version 24 or newer, may work with older versions)
- [Docker Compose](https://docs.docker.com/compose/install/) (version 2 or newer) with the [Docker Compose CLI plugin](https://docs.docker.com/compose/install/linux/)
- [Node.JS](https://nodejs.org/en) (version 18 or higher)
- Yarn Classic (version 1, installed using `npm i -g yarn`)

### Environment setup

Follow the steps from the production environment for setting up the environment file, as well as the port on which the app is being run.

In addition to that, remember to install the dependencies of the project:

```sh
yarn install
```

### Running the development image

Run the app using:

```sh
docker compose up -d
```

Although not required, you should open the app logs by running:

```sh
docker compose logs -f app
```

### Creating and running tests

During development, you might want to write and run tests.

Unit tests are located in the `src` folder, their files are named `*.spec.ts`. They can be run using:

```sh
yarn test
```

End-to-end tests are located in the `test` folder, their files are named `*.e2e-spec.ts`. The can be run using:

```sh
yarn test:e2e
```
