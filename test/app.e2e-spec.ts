import { Test, TestingModule } from '@nestjs/testing'
import { INestApplication } from '@nestjs/common'
import * as request from 'supertest'
import { AppModule } from './../src/app.module'

describe('AppController (e2e)', () => {
  let app: INestApplication

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()
  })

  it('/characters (GET)', () => {
    return request(app.getHttpServer())
      .get('/characters?nameStartsWith=Hulk&page=0&perPage=100')
      .expect(({ body }) => {
        console.log(body)
        expect(body.page).toEqual(0)
        expect(body.count).toBeGreaterThanOrEqual(9) // Could be more in the future
        expect(body.characters.length).toEqual(Math.min(100, body.count))
        expect(body.characters[0].name).toContain('Hulk')
      })
      .expect(200)
  })
})
