import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { CacheModule } from '@nestjs/cache-manager'
import { MarvelApiModule } from './marvel-api/marvel.module'
import { AppController } from './app.controller'

@Module({
  imports: [
    MarvelApiModule,
    ConfigModule.forRoot({ isGlobal: true }),
    CacheModule.register({
      isGlobal: true,
      ttl: 24 * 3600 * 1000, // 24 hours
    }),
  ],
  controllers: [AppController],
})
export class AppModule {}
