import { Test, TestingModule } from '@nestjs/testing'
import { AppController } from './app.controller'
import { AppModule } from './app.module'

describe('AppController', () => {
  let appController: AppController

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    appController = app.get<AppController>(AppController)
  })

  describe('searchMarvelCharacters', () => {
    it('should return multiple characters', async () => {
      const response = await appController.searchMarvelCharacters('Hulk', '0', '100')

      expect(response.page).toBe(0)
      expect(response.count).toBeGreaterThanOrEqual(9) // Could be more in the future
      expect(response.characters.length).toBe(Math.min(100, response.count))
      expect(response.characters[0].name).toContain('Hulk')
    })
  })

  describe('getMarvelCharacter', () => {
    it('should return a character', async () => {
      const character = await appController.getMarvelCharacter('Ant-Man (Hank Pym)')

      expect(character).not.toBeNull()
      expect(character?.name).toEqual('Ant-Man (Hank Pym)')
      expect(character?.comics.length).toBeGreaterThanOrEqual(42) // Could be more in the future
    })

    it('should return null', async () => {
      try {
        await appController.getMarvelCharacter('Definitely not a marvel character')
        fail('This method should have thrown an error.')
      } catch (err) {
        expect(err.response).toEqual('Character not found.')
        expect(err.status).toBe(404)
      }
    })
  })
})
