import { Controller, Get, HttpException, Param, Query, UseInterceptors } from '@nestjs/common'
import { CacheInterceptor } from '@nestjs/cache-manager'
import { MarvelApiService } from './marvel-api/marvel.service'
import { GetMarvelCharacterResponse, SearchMarvelCharactersResponse } from './app.models'

@Controller('/')
export class AppController {
  constructor(private readonly marvelApiService: MarvelApiService) {}

  @Get('/characters')
  @UseInterceptors(CacheInterceptor)
  async searchMarvelCharacters(
    @Query('nameStartsWith') nameStartsWith: string,
    @Query('page') pageQuery = '0',
    @Query('perPage') perPageQuery = '25',
  ): Promise<SearchMarvelCharactersResponse> {
    const page = parseInt(pageQuery)
    const perPage = parseInt(perPageQuery)
    const characters = await this.marvelApiService.findCharactersByName(nameStartsWith, page, perPage)

    return {
      page,
      count: characters.count,
      characters: characters.items.map((it) => ({
        id: it.id,
        name: it.name,
        description: it.description,
      })),
    }
  }

  @Get('/characters/:name')
  @UseInterceptors(CacheInterceptor)
  async getMarvelCharacter(@Param('name') name: string): Promise<GetMarvelCharacterResponse> {
    const character = await this.marvelApiService.getCharacterByName(name)

    if (character === null) {
      throw new HttpException('Character not found.', 404)
    }

    let page = 0
    const { count, items: comics } = await this.marvelApiService.getComicsOfCharacter(character.id, page, 100)

    while (count > comics.length) {
      page += 1

      await this.marvelApiService
        .getComicsOfCharacter(character.id, page, 100)
        .then(({ items }) => items.forEach((it) => comics.push(it)))
    }

    return {
      id: character.id,
      name: character.name,
      description: character.description,
      comics: comics.map((it) => ({
        id: it.id,
        name: it.title,
        description: it.description,
      })),
    }
  }
}
