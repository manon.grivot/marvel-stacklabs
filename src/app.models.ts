export interface MarvelCharacterComic {
  id: number
  name: string
  description: string
}

export interface GetMarvelCharacterResponse {
  id: number
  name: string
  description: string
  comics: MarvelCharacterComic[]
}

export interface SearchMarvelCharactersResponse {
  page: number
  count: number
  characters: Omit<GetMarvelCharacterResponse, 'comics'>[]
}
