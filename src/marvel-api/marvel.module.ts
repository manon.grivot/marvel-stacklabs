import { Module } from '@nestjs/common'
import { MarvelApiService as MarvelApiService } from './marvel.service'

@Module({
  providers: [MarvelApiService],
  exports: [MarvelApiService],
})
export class MarvelApiModule {}
