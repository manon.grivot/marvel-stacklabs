import * as md5 from 'md5'
import axios, { AxiosInstance } from 'axios'
import { HttpException, Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Character, CharactersResponse, Comic, ComicsResponse } from './marvel.models'

interface Counted<T> {
  count: number
  items: T[]
}

@Injectable()
export class MarvelApiService {
  private readonly logger = new Logger(MarvelApiService.name)
  private readonly api: AxiosInstance

  private publicKey: string
  private privateKey: string

  constructor(private readonly configService: ConfigService) {
    this.publicKey = this.configService.get('MARVEL_PUBLIC_KEY') ?? ''
    this.privateKey = this.configService.get('MARVEL_PRIVATE_KEY') ?? ''
    this.api = axios.create({
      baseURL: `https://gateway.marvel.com`,
      params: {
        apikey: this.publicKey,
      },
      headers: {
        Accept: 'application/json',
        Referer: 'localhost',
      },
    })
  }

  private getBaseQueryParams(): { ts: string; hash: string } {
    const ts = Date.now().toFixed()

    return { ts, hash: md5(`${ts}${this.privateKey}${this.publicKey}`) }
  }

  async getComicsOfCharacter(id: number, page: number, perPage: number): Promise<Counted<Comic>> {
    const offset = perPage * page
    const { data: response, status } = await this.api.get<ComicsResponse>(`/v1/public/characters/${id}/comics`, {
      params: {
        ...this.getBaseQueryParams(),
        limit: perPage,
        offset,
      },
    })

    if (status >= 400) {
      throw new HttpException(`Marvel API error ${status}`, 502)
    }

    return {
      count: response.data.total,
      items: response.data.results,
    }
  }

  async getCharacterByName(name: string): Promise<Character | null> {
    const { data: response, status } = await this.api.get<CharactersResponse>(`/v1/public/characters`, {
      params: {
        ...this.getBaseQueryParams(),
        name,
      },
    })

    if (status >= 400) {
      throw new HttpException(`Marvel API error ${status}`, 502)
    }

    if (response.data.results.length > 0) {
      return response.data.results[0]
    }
    return null
  }

  async findCharactersByName(name: string, page: number, perPage: number): Promise<Counted<Character>> {
    const offset = perPage * page
    const { data: response, status } = await this.api.get<CharactersResponse>(`/v1/public/characters`, {
      params: {
        ...this.getBaseQueryParams(),
        nameStartsWith: name,
        limit: perPage,
        offset,
      },
    })

    if (status >= 400) {
      throw new HttpException(`Marvel API error ${status}`, 502)
    }

    return {
      count: response.data.total,
      items: response.data.results,
    }
  }
}
