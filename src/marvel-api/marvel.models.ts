export interface MarvelError {
  code: string
  message: string
}

export interface MarvelResponse<T> {
  code: number
  status: string
  copyright?: string
  attributionText?: string
  attributionHTML?: string
  data: T
  etag?: string
}

export interface DataList<T> {
  available: number
  returned: number
  collectionURI: string
  items: T[]
}

export interface DataContainer<T> {
  offset: number
  limit: number
  total: number
  count: number
  results: T[]
}

export interface Url {
  type: string
  url: string
}

export interface Image {
  path: string
  extension: string
}

export interface CharacterSummary {
  resourceURI: string
  name: string
  role: string
}

export interface ComicSummary {
  resourceURI: string
  name: string
}

export interface StorySummary {
  resourceURI: string
  name: string
  type: string
}

export interface EventSummary {
  resourceURI: string
  name: string
}

export interface SeriesSummary {
  resourceURI: string
  name: string
}

export interface Character {
  id: number
  name: string
  description: string
  modified: Date
  resourceURI: string
  urls: Url[]
  thumbnail: Image
  comics: DataList<ComicSummary>
  stories: DataList<StorySummary>
  events: DataList<EventSummary>
  series: DataList<SeriesSummary>
}

export interface Comic {
  id: number
  digitalId: number
  title: string
  issueNumber: number
  variantDescription?: string
  description: string
  modified: Date
  isbn: string
  upc: string
  diamondCode: string
  ean: string
  issn: string
  format: string
  pageCount: number
  textObjects: unknown[]
  resourceURI: string
  urls: Url[]
  series: SeriesSummary
  variants: ComicSummary[]
  collections: ComicSummary[]
  collectedIssues: ComicSummary[]
  dates: unknown[]
  prices: unknown[]
  thumbnail: Image
  images: Image[]
  creators: DataList<unknown>
  characters: DataList<CharacterSummary>
  stories: DataList<StorySummary>
  events: DataList<EventSummary>
}

export type CharactersResponse = MarvelResponse<DataContainer<Character>>

export type ComicsResponse = MarvelResponse<DataContainer<Comic>>
