import { Test, TestingModule } from '@nestjs/testing'
import { ConfigModule } from '@nestjs/config'
import { CacheModule } from '@nestjs/cache-manager'
import { MarvelApiService } from './marvel.service'

describe('MarvelModule', () => {
  let service: MarvelApiService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot(), CacheModule.register()],
      providers: [MarvelApiService],
    }).compile()

    service = module.get(MarvelApiService)
  })

  describe('getComicsOfCharacter', () => {
    it('should return a list of comics', async () => {
      const comics = await service.getComicsOfCharacter(1009351, 0, 5)

      expect(comics.count).toBeGreaterThan(1500) // Actual number will change constantly
      expect(comics.items.length).toBe(5)
      expect(comics.items[0].isbn).not.toBeTruthy() // Comics are the only ones to have this property
    })
  })

  describe('getCharacterByName', () => {
    it('should return a character', async () => {
      const character = await service.getCharacterByName('Iron Man')

      expect(character).not.toBeNull()
      expect(character?.name).toContain('Iron Man')
    })

    it('should return null', async () => {
      const character = await service.getCharacterByName('Definitely not a marvel character')

      expect(character).toBeNull()
    })
  })

  describe('findCharactersByName', () => {
    it('should return multiple characters', async () => {
      const res = await service.findCharactersByName('Agent', 0, 100)

      expect(res.count).toBeGreaterThanOrEqual(4) // Could be more in the future
      expect(res.items.length).toBe(res.count)
    })

    it('should paginate results', async () => {
      const firstPage = await service.findCharactersByName('Hulk', 0, 8)
      const secondPage = await service.findCharactersByName('Hulk', 1, 8)

      expect(firstPage.count).toBeGreaterThanOrEqual(9) // Could be more in the future
      expect(firstPage.items.length).toBe(8)
      expect(secondPage.count).toBe(firstPage.count)
      expect(secondPage.items.length).toBe(Math.min(8, secondPage.count - 8)) // Either 8 (perPage) or the number of remaining elements
    })
  })
})
