### Dev target
FROM node:20 AS dev

EXPOSE 3000

WORKDIR /home/node/app
USER node

VOLUME /home/node/app

ENTRYPOINT ["yarn", "start:dev"]

### Pre-build of the prod target
FROM node:20 AS build

WORKDIR /home/node/app

COPY . .

RUN yarn install \
  && yarn build

### Prod target
FROM node:20 AS prod

ENV NODE_ENV=production
EXPOSE 3000

WORKDIR /home/node/app

COPY ./package.json ./yarn.lock ./
COPY --from=build /home/node/app/dist ./dist

RUN yarn install --production

ENTRYPOINT ["yarn", "start:prod"]
